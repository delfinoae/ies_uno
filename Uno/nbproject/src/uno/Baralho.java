package uno;
import java.util.ArrayList;
import java.util.Collections;

public class Baralho implements Pilha {
	public int size; //variavel de tamalho
	private ArrayList<Carta> baralho;
	//cria todas as cartas
	private String  azul, um, dois, tres, quatro, cinco, seis, sete, oito, nove, zero, amarelo, vermelho,verde, preto, block, mais4, mais2, coringa, inverte;
	private Carta inverteAzul =new Carta(azul, inverte);
	private Carta inverteAmarelo =new Carta(amarelo, inverte);
	private Carta inverteVermelho =new Carta(vermelho, inverte);
	private Carta inverteVerde =new Carta(verde, inverte);
	private Carta blockVerde =new Carta(verde, block);
	private Carta blockVermelho =new Carta(vermelho, block);
	private Carta blockAmarelo =new Carta(amarelo, block);
	private Carta blockAzul =new Carta(azul, block);
	private Carta mais2Azul =new Carta(azul, mais2);
	private Carta mais2Amarelo =new Carta(amarelo, mais2);
	private Carta mais2Verde =new Carta(verde, mais2);
	private Carta mais2Vermelho =new Carta(vermelho, mais2);
	private Carta coringaC =new Carta(preto, coringa);
	private Carta mais4C =new Carta(preto, mais4);
	private Carta umAzul =new Carta(azul, um);
	private Carta umAmarelo =new Carta(amarelo, um);
	private Carta umVerde =new Carta(verde, um);
	private Carta umVermelho =new Carta(vermelho, um);
	private Carta doisAzul =new Carta(azul, dois);
	private Carta doisAmarelo =new Carta(amarelo, dois);
	private Carta doisVerde =new Carta(verde, dois);
	private Carta doisVermelho =new Carta(vermelho, dois);
	private Carta tresAzul =new Carta(azul, tres);
	private Carta tresAmarelo =new Carta(amarelo, tres);
	private Carta tresVerde =new Carta(verde, tres);
	private Carta tresVermelho =new Carta(vermelho, tres);
	private Carta quatroAzul =new Carta(azul, quatro);
	private Carta quatroAmarelo =new Carta(amarelo, quatro);
	private Carta quatroVerde =new Carta(verde, quatro);
	private Carta quatroVermelho =new Carta(vermelho, quatro);
	private Carta cincoAzul =new Carta(azul, cinco);
	private Carta cincoAmarelo =new Carta(amarelo, cinco);
	private Carta cincoVerde =new Carta(verde, cinco);
	private Carta cincoVermelho =new Carta(vermelho, cinco);
	private Carta seisAzul =new Carta(azul, seis);
	private Carta seisAmarelo =new Carta(amarelo, seis);
	private Carta seisVerde =new Carta(verde, seis);
	private Carta seisVermelho =new Carta(vermelho, seis);
	private Carta seteAzul =new Carta(azul, sete);
	private Carta seteAmarelo =new Carta(amarelo, sete);
	private Carta seteVerde =new Carta(verde, sete);
	private Carta seteVermelho =new Carta(vermelho, sete);
	private Carta oitoAzul =new Carta(azul,oito);
	private Carta oitoAmarelo =new Carta(amarelo,oito);
	private Carta oitoVerde =new Carta(verde,oito);
	private Carta oitoVermelho =new Carta(vermelho,oito);
	private Carta noveAzul =new Carta(azul,nove);
	private Carta noveAmarelo =new Carta(amarelo,nove);
	private Carta noveVerde =new Carta(verde,nove);
	private Carta noveVermelho =new Carta(vermelho, nove);
	private Carta zeroAzul =new Carta(azul,zero);
	private Carta zeroAmarelo =new Carta(amarelo,zero);
	private Carta zeroVerde =new Carta(verde,zero);
	private Carta zeroVermelho =new Carta(vermelho,zero);
	
	public Baralho(){// contrutor, adiciona as cartas e embaralha
		baralho = new ArrayList(108);
		push(zeroVermelho);
		push(zeroVerde);	
		push(zeroAmarelo);	
		push(zeroAzul);		
		push(noveAzul);		push(noveAzul);
		push(noveAmarelo);	push(noveAmarelo);
		push(noveVerde);    push(noveVerde);
		push(noveVermelho);	push(noveVermelho);
		push(oitoAzul);		push(oitoAzul);
		push(oitoAmarelo);	push(oitoAmarelo);
		push(oitoVerde);	push(oitoVerde);
		push(oitoVermelho); push(oitoVermelho);
		push(seteAzul);		push(oitoAzul);
		push(seteAmarelo);	push(seteAmarelo);
		push(seteVerde);	push(seteVerde);
		push(seteVermelho);	push(seteVermelho);
		push(seisAzul);		push(seisAzul);
		push(seisAmarelo);	push(seisAmarelo);
		push(seisVerde);	push(seisVerde);
		push(seisVermelho);	push(seisVermelho);
		push(cincoAzul);	push(cincoAzul);
		push(cincoAmarelo);	push(cincoAmarelo);
		push(cincoVerde);	push(cincoVerde);
		push(cincoVermelho);push(cincoVermelho);
		push(quatroAzul);	push(quatroAzul);
		push(quatroAmarelo);push(quatroAmarelo);
		push(quatroVerde);	push(quatroVerde);
		push(quatroVermelho);push(quatroVermelho);
		push(tresAzul);		push(tresAzul);
		push(tresAmarelo);	push(tresAmarelo);
		push(tresVerde);	push(tresVerde);
		push(tresVermelho);	push(tresVermelho);
		push(doisAzul);		push(doisAzul);
		push(doisAmarelo);	push(doisAmarelo);
		push(doisVerde);	push(doisVerde);
		push(doisVermelho);	push(doisVermelho);
		push(umAzul);		push(umAzul);
		push(umAmarelo);	push(umAmarelo);
		push(umVerde);		push(umVerde);
		push(umVermelho);	push(umVermelho);
		push(mais4C); push(mais4C); push(mais4C); push(mais4C);
		push(coringaC); push(coringaC); push(coringaC); push(coringaC);
		push(mais2Vermelho);push(mais2Vermelho);
		push(mais2Verde);	push(mais2Verde);
		push(mais2Amarelo);	push(mais2Amarelo);
		push(mais2Azul);	push(mais2Azul);
		push(blockAzul);	push(blockAzul);
		push(blockAmarelo);	push(blockAmarelo);
		push(blockVerde);	push(blockVerde);
		push(blockVermelho);push(blockVermelho);
		push(inverteAzul);	push(inverteAzul);
		push(inverteAmarelo);push(inverteAmarelo);
		push(inverteVerde);	push(inverteVerde);
		push(inverteVermelho);push(inverteVermelho);
		Collections.shuffle(baralho);
		}
	
	public void push(Carta x){//adiciona carta 
		if(x==null)throw new IllegalArgumentException("Escolha uma carta");    
		size++;
		baralho.add(x); 
	}
	public Carta pop(){//remove carta
		if(isEmpty()==true)    
			throw new IllegalArgumentException("Não tem uma carta ai não hein");
		size--;	
		return baralho.remove(0);
	}
	public boolean isEmpty() {  //verifica se esta vazio
		return size<=0;
	}
	public int getSize() {    //mostra tamanho
		return size;
	}
}
	