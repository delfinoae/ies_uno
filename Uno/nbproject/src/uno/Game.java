package uno;

import java.util.Scanner;

public class Game {
	boolean aux=false; //auxiliar do caso block jogador 2
	//instancia��o dos objetos
	Baralho baralho=new Baralho();
	Mao mao1=new Mao();
	Mao mao2=new Mao();
	Morto morto=new Morto();
	Scanner terminal = new Scanner(System.in);
	public void game(){ //metodo que cria o jogo
		//dando as cartas
		mao1.push(baralho.pop());
		mao1.push(baralho.pop());
		mao1.push(baralho.pop());
		mao1.push(baralho.pop());
		mao1.push(baralho.pop());
		mao1.push(baralho.pop());
		mao1.push(baralho.pop());
		mao2.push(baralho.pop());
		mao2.push(baralho.pop());
		mao2.push(baralho.pop());
		mao2.push(baralho.pop());
		mao2.push(baralho.pop());
		mao2.push(baralho.pop());
		mao2.push(baralho.pop());
		while(mao1.isEmpty() || mao2.isEmpty()){ //enquanto uma mao n�o estiver vazia

			if(morto.mostraTop().getTipo()!="block" || aux==false){//verifica se n foi bloqueado
				mao1.mostraMao();//mostra mao
				aux=false; //auxiliar do caso block
				boolean aux1=false; //auxiliar para decidir se o jogador tem uma carta valida
				for(int i=0;i<=mao1.size();i++){
						//verifica se o jogador tem uma carta valida
					if(morto.mostraTop().getTipo().equal(mao1.getTipo(i)) || morto.mostraTop().getCor().equal(mao1.getCor(i)) || mao1.getTipo(i).equals("coringa") || mao1.getTipo(i).equals("mais4"))
						aux1=true
				}
				if(aux1){
					System.out.println("jogador 1 joga: Selecione uma carta (digite a posi��o da carta desejada)");
					int card=terminal.nextInt();
					morto.push(mao1.remove(card)); //morto recebe a carta que o jogador 1 escolheu
					if(morto.mostraTop().getTipo()=="block"){ //se a carta for um bloqueio avisa na tela e volta pro inicio do while
						System.out.println("jogador 2 foi bloqueado, jogador 1 joga de novo");
						System.out.println("carta no topo do monte: " + morto.mostraTop());
						continue;

					}
					if(morto.mostraTop().getTipo()=="mais4"){//se a carta for mais 4 adversario recebe quatro cartas e jogador 1 escolhe uma nova cor
						mao2.push(baralho.pop());
						mao2.push(baralho.pop());
						mao2.push(baralho.pop());
						mao2.push(baralho.pop());
						System.out.println("selecione uma cor: ");
						String cor = terminal.nextLine();
						morto.mostraTop().setCor(cor);
					}
					if(morto.mostraTop().getTipo()=="mais2"){//se a carta for mais 2 adversario recebe duas cartas
						mao2.push(baralho.pop());
						mao2.push(baralho.pop());
					}
					if(morto.mostraTop().getTipo()=="coringa"){ //se for coringa, jogador escolhe nova cor
						System.out.println("selecione uma cor: ");
						String cor = terminal.nextLine();
						morto.mostraTop().setCor(cor);
					}
				}
				else{ System.out.println("voc� n�o tem uma carta v�lida para jogar, tome aqui uma carta nova para se sentir feliz :)");
				mao1.push(baralho.pop());
				}
			}
			//mesmo codigo, s� que pro jogador 2
			System.out.println("carta no topo do monte: " + morto.mostraTop());
			aux=true;
			mao2.mostraMao();
			for(int i=0;i<=mao2.size();i++){
					//verifica se o jogador tem uma carta valda
				if(morto.mostraTop().getTipo().equal(mao2.getTipo(i)) || morto.mostraTop().getCor().equal(mao2.getCor(i)) || mao2.getTipo(i).equals("coringa") || mao2.getTipo(i).equals("mais4"))
					aux1=true
			}
			if(aux1){
				System.out.println("jogador 2 joga: Selecione uma carta (digite a posi��o da carta desejada)");
				int card1=terminal.nextInt();
				morto.push(mao2.remove(card));
				System.out.println("carta no topo do monte: " + morto.mostraTop());
				if(morto.mostraTop().getTipo()=="block"){
					System.out.println("jogador 1 foi bloqueado, jogador 2 joga de novo");
				}
				if(morto.mostraTop().getTipo()=="mais4"){
					mao1.push(baralho.pop());
					mao1.push(baralho.pop());
					mao1.push(baralho.pop());
					mao1.push(baralho.pop());
					System.out.println("selecione uma cor: ");
					String cor = terminal.nextLine();
					morto.mostraTop().setCor(cor);
				}
				if(morto.mostraTop().getTipo()=="mais2"){
					mao2.push(baralho.pop());
					mao2.push(baralho.pop());
				}
				if(morto.mostraTop().getTipo()=="coringa"){
					System.out.println("selecione uma cor: ");
					String cor = terminal.nextLine();
					morto.mostraTop().setCor(cor);
				}

			}
			else{ System.out.println("voc� n�o tem uma carta v�lida para jogar, tome aqui uma carta nova para se sentir feliz :)");
			mao2.push(baralho.pop());
			}
		}
		if(!mao1.isEmpty())// mostra o ganhador
			System.out.println("ganhou jogador 1");
		else System.out.println("ganhou jogador 2");

	}

}











