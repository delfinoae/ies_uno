package uno;
import java.util.ArrayList;
public class Morto implements Pilha {
	private int size;
	private ArrayList<Carta> morto;

	public Morto(){//construtor
		morto = new ArrayList();
	}
	public void push(Carta x){//adiciona carta, tem um throws ali por motivos de for�a maior
		if(x==null)throw new IllegalArgumentException("Escolha uma carta");    
		size++;
		morto.add(x); 
	}
	public Carta pop(){//remove carta, tem um throws ali por motivos de for�a maior
		if(isEmpty()==true)    
			throw new IllegalArgumentException("Não tem uma carta ai não hein");
		size--;	
		return morto.remove(0);
	}
	
	public Carta mostraTop(){//mostra carta de cima
		Carta aux=pop();
		push(aux);
		return aux;
	}
	public boolean isEmpty() {  //avisa se ta vazio
		return size<=0;
	}
}