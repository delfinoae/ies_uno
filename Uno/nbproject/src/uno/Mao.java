package uno;
import java.util.ArrayList;
public class Mao {
	private ArrayList<Carta> mao;
	private int size; 
	public Mao(){//contrutor
		mao = new ArrayList(7);
	}
	public void push(Carta x){//metodo que adiciona uma carta a mao (botei um throws por motivos de for�a maior)
		if(x==null)throw new IllegalArgumentException("Escolha uma carta");    
		size++;
		mao.add(x); 
	}
	public Carta remove(int x){//metodo que remove uma carta (botei um throws por motivos de for�a maior)
		if(isEmpty()==true)    
			throw new IllegalArgumentException("Não tem uma carta ai n�o hein");
		size--;	
		return mao.remove(x);
	}


	public boolean isEmpty(){//verifica se esta vazio  
		return size<=0;
	}

	public void mostraMao(){// mostra a mao do jogador
		for (int i=0; i<=mao.size();i++){
			System.out.println(mao.get(i));
		}
	}
}
