package uno;

public class Carta {
String cor, tipo, nome;

	public Carta(String cor, String tipo){//construtor
		this.cor=cor;
		this.tipo=tipo;
			}

	public String getCor() {//retorna a cor
		return cor;
	}

	public void setCor(String cor) {//define cor
		this.cor = cor;
	}
	
	public String getTipo() {//retorna tipo
		return tipo;
	}

	public void setTipo(String tipo) {// define tipo
		this.tipo = tipo;
	}
}
